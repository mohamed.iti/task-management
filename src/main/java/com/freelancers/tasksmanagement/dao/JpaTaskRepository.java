package com.freelancers.tasksmanagement.dao;

import com.freelancers.tasksmanagement.model.Task;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JpaTaskRepository extends JpaRepository<Task,Long> {
}
