package com.freelancers.tasksmanagement.dao;

import com.freelancers.tasksmanagement.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JpaEmployeeRepository extends JpaRepository<Employee,Long> {
}
